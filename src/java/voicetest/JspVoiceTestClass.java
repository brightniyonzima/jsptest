package voicetest;

/* Import SDK classes */
import com.africastalking.Callback;
import com.africastalking.AfricasTalking;
import com.africastalking.VoiceService;
import com.africastalking.voice.CallResponse;

import java.io.IOException;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lenovo
 */
public class JspVoiceTestClass {
    public static void main(String[] args)
    {
	/* Set your app credentials */
	//String USERNAME = "sandbox";
	//String API_KEY = "16fc3e8992f3f4278e5fa98060a6900f933be33ffbac8e1333c86202b4dd3fc7";
        
        /* details used by my app named itrial12*/
        String USERNAME = "trial027";
	String API_KEY = "7bc1683f5529bfdffd7d627b00aa9c5b474fc701f2d6e1f272d15a852c6b2c64";

	/* Initialize SDK */
	AfricasTalking.initialize(USERNAME, API_KEY);

	/* Get the voice service */
	VoiceService voice = AfricasTalking.getService(AfricasTalking.SERVICE_VOICE);

	/* Set your Africa's Talking phone number in international format */
        String callerId = "+256312319994";

        /* Set the numbers you want to call to in a comma-separated list */
        String phoneNumber = "+256775040213";

	/* Make the call */
	try {
            
	   CallResponse response = voice.call(phoneNumber, callerId);
           System.out.println(response.toString());
	} catch(Exception ex) {
 
	   ex.printStackTrace();
	}
   }   
}


